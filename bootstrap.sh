#!/usr/bin/env bash
sudo apt-get update
sudo apt-get -y upgrade
sudo apt-get -y install curl
sudo apt-get -y install openjdk-8-jdk
# Java
echo "JAVA_HOME=/usr" >> /etc/profile
echo 'PATH=$PATH:$HOME/bin:$JAVA_HOME/bin' >> /etc/profile
echo "export JAVA_HOME" >> /etc/profile
echo "export PATH" >> /etc/profile
. /etc/profile

# Hadoop user
addgroup hadoop_
adduser --disabled-password --gecos "hadoop,1,1,1,1" --ingroup hadoop_ hduser_
adduser hduser_ sudo
sudo su -c 'echo "hduser_     ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers'

# Hadoop install

sudo su hduser_ -c 'mkdir ~/inputMapReduce'

# Keys
sudo su hduser_ -c 'ssh-keygen -t rsa -f /home/hduser_/.ssh/id_rsa -N ""'
sudo su hduser_ -c 'cat $HOME/.ssh/id_rsa.pub >> $HOME/.ssh/authorized_keys'

# Hadoop
sudo tar zxvf /home/vagrant/fifa/installationFiles/hadoop-2.8.3-configured.tar.gz -C /home/hduser_/
sudo chown -R hduser_ /home/hduser_/hadoop

sudo mkdir -p /home/hduser_/hdfs
sudo chown -R hduser_:hadoop_ /home/hduser_/hdfs
sudo chmod 750 /home/hduser_/hdfs


sudo su hduser_ -c 'echo "JAVA_HOME=/usr" >> ~/.bashrc'
sudo su hduser_ -c 'echo "export HADOOP_HOME=~/hadoop" >> ~/.bashrc'
sudo su hduser_ -c 'echo '"'"'export PATH=$PATH:$HADOOP_HOME/bin'"'"' >> ~/.bashrc'

sudo su -c 'echo "export HADOOP_HOME=~/hadoop" >> /etc/profile.d/hadoop.sh'
sudo su -c 'chmod +x /etc/profile.d/hadoop.sh'

# Hive
wget http://mirrors.netix.net/apache/hive/hive-2.3.6/apache-hive-2.3.6-bin.tar.gz
sudo tar zxvf apache-hive-2.3.6-bin.tar.gz
sudo mv apache-hive-2.3.6-bin /home/hduser_/hive
sudo rm apache-hive-2.3.6-bin.tar.gz

sudo cp /home/vagrant/fifa/installationFiles/hive/hive-env.sh /home/hduser_/hive/conf/

sudo su hduser_ -c 'echo "export HIVE_HOME=~/hive" >> ~/.bashrc'
sudo su hduser_ -c 'echo '"'"'export PATH=$PATH:$HIVE_HOME/bin'"'"' >> ~/.bashrc'
sudo su hduser_ -c 'echo '"'"'export CLASSPATH=$CLASSPATH:$HADOOP_HOME/bin/*:.'"'"' >> ~/.bashrc'
sudo su hduser_ -c 'echo '"'"'export CLASSPATH=$CLASSPATH:~/hive/lib/*:.'"'"' >> ~/.bashrc'

# MySQL
sudo apt-get -y install mysql-server
sudo apt-get -y install libmysql-java

sudo bash /home/vagrant/fifa/installationFiles/mysqlinstall.sh 'root'

sudo su hduser_ -c 'sudo ln -s /usr/share/java/mysql-connector-java.jar /home/hduser_/hive/lib/mysql-connector-java.jar'
sudo cp /home/vagrant/fifa/installationFiles/hive/hive-site.xml /home/hduser_/hive/conf/

sudo su hduser_ -c 'echo "export HIVE_CONF_DIR=/home/hduser_/hive/conf" >> ~/.bashrc'

# Grant Hive permissions
sudo chown -R hduser_ /home/hduser_/hive

# Sqoop
wget http://archive.apache.org/dist/sqoop/1.4.7/sqoop-1.4.7.bin__hadoop-2.6.0.tar.gz
tar zxvf sqoop-1.4.7.bin__hadoop-2.6.0.tar.gz
sudo mv sqoop-1.4.7.bin__hadoop-2.6.0 /home/hduser_/sqoop
sudo rm sqoop-1.4.7.bin__hadoop-2.6.0.tar.gz

sudo su hduser_ -c 'echo "export SQOOP_HOME=/home/hduser_/sqoop" >> ~/.bashrc'
sudo su hduser_ -c 'echo '"'"'export PATH=$PATH:$SQOOP_HOME/bin'"'"' >> ~/.bashrc'
sudo su hduser_ -c 'echo '"'"'export HADOOP_CLASSPATH=$HADOOP_CLASSPATH:/home/hduser_/sqoop/lib/*'"'"' >> ~/.bashrc'


sudo cp /home/vagrant/fifa/installationFiles/sqoop/sqoop-env.sh /home/hduser_/sqoop/conf/

wget https://jdbc.postgresql.org/download/postgresql-42.2.8.jar
sudo mv postgresql-42.2.8.jar /home/hduser_/sqoop/lib

sudo rm /home/hduser_/sqoop/lib/jackson-*-2.3.1.jar
sudo cp /home/vagrant/fifa/installationFiles/sqoop/jackson-*-2.10.0.jar /home/hduser_/sqoop/lib/
sudo cp /home/vagrant/fifa/installationFiles/hive/derby-10.10.1.1.jar /home/hduser_/sqoop/lib

sudo chown -R hduser_:hadoop_ /home/hduser_/sqoop

# Vagrant conf
echo "export WORKON_HOME=~/Envs" >> /home/vagrant/.profile
echo "cd /home/vagrant/fifa/" >> /home/vagrant/.profile
echo "sudo su hduser_" >> /home/vagrant/.profile

echo "*******************************************************"
echo "Please, login using vagrant ssh and run ./config/finishSetup.sh"
echo "*******************************************************"
