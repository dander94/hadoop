package player;

import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;

public class PlayerResultSortComparator extends WritableComparator {

    public PlayerResultSortComparator() {
        super(PlayerResult.class, true);
    }

     @Override
     public int compare(WritableComparable wc1, WritableComparable wc2) {
         PlayerResult result = (PlayerResult) wc1;
         PlayerResult result2 = (PlayerResult) wc2;
         int compareValue = result.getValue().compareTo(result2.getValue());
         if (compareValue == 0) {
           compareValue = result.getName().compareTo(result2.getName());
         }
         return compareValue;
     }
 }
