package player;

import java.util.Map;

public class Player {

  private String id;
  private String name;
  private Integer age;
  private Integer potential;
  private String club;
  private String position;
  private Integer crossing;
  private Integer finishing;
  private Integer headingaccuracy;
  private Integer shortpassing;
  private Integer volleys;
  private Integer dribbling;
  private Integer curve;
  private Integer fkaccuracy;
  private Integer longpassing;
  private Integer ballcontrol;
  private Integer acceleration;
  private Integer sprintspeed;
  private Integer agility;
  private Integer reactions;
  private Integer balance;
  private Integer shotpower;
  private Integer jumping;
  private Integer stamina;
  private Integer strength;
  private Integer longshots;
  private Integer aggression;
  private Integer interceptions;
  private Integer positioning;
  private Integer vision;
  private Integer penalties;
  private Integer composure;
  private Integer marking;
  private Integer standingtackle;
  private Integer slidingtackle;
  private Integer gkdiving;
  private Integer gkhandling;
  private Integer gkkicking;
  private Integer gkpositioning;
  private Integer gkreflexes;

  /**
  * No args constructor for use in serialization
  *
  */
  public Player() {
  }

  /**
  *
  * @param jumping
  * @param strength
  * @param penalties
  * @param composure
  * @param curve
  * @param stamina
  * @param longshots
  * @param volleys
  * @param ballcontrol
  * @param slidingtackle
  * @param gkkicking
  * @param gkreflexes
  * @param gkhandling
  * @param finishing
  * @param acceleration
  * @param dribbling
  * @param balance
  * @param positioning
  * @param shotpower
  * @param club
  * @param id
  * @param potential
  * @param gkdiving
  * @param longpassing
  * @param aggression
  * @param standingtackle
  * @param crossing
  * @param interceptions
  * @param shortpassing
  * @param fkaccuracy
  * @param vision
  * @param marking
  * @param gkpositioning
  * @param sprintspeed
  * @param name
  * @param headingaccuracy
  * @param reactions
  * @param position
  * @param agility
  * @param age
  */
  public Player(String id, String name, Integer age, Integer potential, String club, String position, Integer crossing, Integer finishing, Integer headingaccuracy, Integer shortpassing, Integer volleys, Integer dribbling, Integer curve, Integer fkaccuracy, Integer longpassing, Integer ballcontrol, Integer acceleration, Integer sprintspeed, Integer agility, Integer reactions, Integer balance, Integer shotpower, Integer jumping, Integer stamina, Integer strength, Integer longshots, Integer aggression, Integer interceptions, Integer positioning, Integer vision, Integer penalties, Integer composure, Integer marking, Integer standingtackle, Integer slidingtackle, Integer gkdiving, Integer gkhandling, Integer gkkicking, Integer gkpositioning, Integer gkreflexes) {
    super();
    this.id = id;
    this.name = name;
    this.age = age;
    this.potential = potential;
    this.club = club;
    this.position = position;
    this.crossing = crossing;
    this.finishing = finishing;
    this.headingaccuracy = headingaccuracy;
    this.shortpassing = shortpassing;
    this.volleys = volleys;
    this.dribbling = dribbling;
    this.curve = curve;
    this.fkaccuracy = fkaccuracy;
    this.longpassing = longpassing;
    this.ballcontrol = ballcontrol;
    this.acceleration = acceleration;
    this.sprintspeed = sprintspeed;
    this.agility = agility;
    this.reactions = reactions;
    this.balance = balance;
    this.shotpower = shotpower;
    this.jumping = jumping;
    this.stamina = stamina;
    this.strength = strength;
    this.longshots = longshots;
    this.aggression = aggression;
    this.interceptions = interceptions;
    this.positioning = positioning;
    this.vision = vision;
    this.penalties = penalties;
    this.composure = composure;
    this.marking = marking;
    this.standingtackle = standingtackle;
    this.slidingtackle = slidingtackle;
    this.gkdiving = gkdiving;
    this.gkhandling = gkhandling;
    this.gkkicking = gkkicking;
    this.gkpositioning = gkpositioning;
    this.gkreflexes = gkreflexes;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Integer getAge() {
    return age;
  }

  public void setAge(Integer age) {
    this.age = age;
  }

  public Integer getPotential() {
    return potential;
  }

  public void setPotential(Integer potential) {
    this.potential = potential;
  }

  public String getClub() {
    return club;
  }

  public void setClub(String club) {
    this.club = club;
  }

  public String getPosition() {
    return position;
  }

  public void setPosition(String position) {
    this.position = position;
  }

  public Integer getCrossing() {
    return crossing;
  }

  public void setCrossing(Integer crossing) {
    this.crossing = crossing;
  }

  public Integer getFinishing() {
    return finishing;
  }

  public void setFinishing(Integer finishing) {
    this.finishing = finishing;
  }

  public Integer getHeadingaccuracy() {
    return headingaccuracy;
  }

  public void setHeadingaccuracy(Integer headingaccuracy) {
    this.headingaccuracy = headingaccuracy;
  }

  public Integer getShortpassing() {
    return shortpassing;
  }

  public void setShortpassing(Integer shortpassing) {
    this.shortpassing = shortpassing;
  }

  public Integer getVolleys() {
    return volleys;
  }

  public void setVolleys(Integer volleys) {
    this.volleys = volleys;
  }

  public Integer getDribbling() {
    return dribbling;
  }

  public void setDribbling(Integer dribbling) {
    this.dribbling = dribbling;
  }

  public Integer getCurve() {
    return curve;
  }

  public void setCurve(Integer curve) {
    this.curve = curve;
  }

  public Integer getFkaccuracy() {
    return fkaccuracy;
  }

  public void setFkaccuracy(Integer fkaccuracy) {
    this.fkaccuracy = fkaccuracy;
  }

  public Integer getLongpassing() {
    return longpassing;
  }

  public void setLongpassing(Integer longpassing) {
    this.longpassing = longpassing;
  }

  public Integer getBallcontrol() {
    return ballcontrol;
  }

  public void setBallcontrol(Integer ballcontrol) {
    this.ballcontrol = ballcontrol;
  }

  public Integer getAcceleration() {
    return acceleration;
  }

  public void setAcceleration(Integer acceleration) {
    this.acceleration = acceleration;
  }

  public Integer getSprintspeed() {
    return sprintspeed;
  }

  public void setSprintspeed(Integer sprintspeed) {
    this.sprintspeed = sprintspeed;
  }

  public Integer getAgility() {
    return agility;
  }

  public void setAgility(Integer agility) {
    this.agility = agility;
  }

  public Integer getReactions() {
    return reactions;
  }

  public void setReactions(Integer reactions) {
    this.reactions = reactions;
  }

  public Integer getBalance() {
    return balance;
  }

  public void setBalance(Integer balance) {
    this.balance = balance;
  }

  public Integer getShotpower() {
    return shotpower;
  }

  public void setShotpower(Integer shotpower) {
    this.shotpower = shotpower;
  }

  public Integer getJumping() {
    return jumping;
  }

  public void setJumping(Integer jumping) {
    this.jumping = jumping;
  }

  public Integer getStamina() {
    return stamina;
  }

  public void setStamina(Integer stamina) {
    this.stamina = stamina;
  }

  public Integer getStrength() {
    return strength;
  }

  public void setStrength(Integer strength) {
    this.strength = strength;
  }

  public Integer getLongshots() {
    return longshots;
  }

  public void setLongshots(Integer longshots) {
    this.longshots = longshots;
  }

  public Integer getAggression() {
    return aggression;
  }

  public void setAggression(Integer aggression) {
    this.aggression = aggression;
  }

  public Integer getInterceptions() {
    return interceptions;
  }

  public void setInterceptions(Integer interceptions) {
    this.interceptions = interceptions;
  }

  public Integer getPositioning() {
    return positioning;
  }

  public void setPositioning(Integer positioning) {
    this.positioning = positioning;
  }

  public Integer getVision() {
    return vision;
  }

  public void setVision(Integer vision) {
    this.vision = vision;
  }

  public Integer getPenalties() {
    return penalties;
  }

  public void setPenalties(Integer penalties) {
    this.penalties = penalties;
  }

  public Integer getComposure() {
    return composure;
  }

  public void setComposure(Integer composure) {
    this.composure = composure;
  }

  public Integer getMarking() {
    return marking;
  }

  public void setMarking(Integer marking) {
    this.marking = marking;
  }

  public Integer getStandingtackle() {
    return standingtackle;
  }

  public void setStandingtackle(Integer standingtackle) {
    this.standingtackle = standingtackle;
  }

  public Integer getSlidingtackle() {
    return slidingtackle;
  }

  public void setSlidingtackle(Integer slidingtackle) {
    this.slidingtackle = slidingtackle;
  }

  public Integer getGkdiving() {
    return gkdiving;
  }

  public void setGkdiving(Integer gkdiving) {
    this.gkdiving = gkdiving;
  }

  public Integer getGkhandling() {
    return gkhandling;
  }

  public void setGkhandling(Integer gkhandling) {
    this.gkhandling = gkhandling;
  }

  public Integer getGkkicking() {
    return gkkicking;
  }

  public void setGkkicking(Integer gkkicking) {
    this.gkkicking = gkkicking;
  }

  public Integer getGkpositioning() {
    return gkpositioning;
  }

  public void setGkpositioning(Integer gkpositioning) {
    this.gkpositioning = gkpositioning;
  }

  public Integer getGkreflexes() {
    return gkreflexes;
  }

  public void setGkreflexes(Integer gkreflexes) {
    this.gkreflexes = gkreflexes;
  }

}
