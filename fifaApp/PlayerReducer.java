package player;

import java.io.IOException;
import java.util.*;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import static player.PlayerConfiguration.RESULTS_TOLERANCE;

public class PlayerReducer extends Reducer<PlayerResult, DoubleWritable, Text, DoubleWritable> {

	public void reduce(PlayerResult key, Iterable<DoubleWritable> values,Context context) throws IOException,InterruptedException {
		double playerSimilarity = 0;

		for(DoubleWritable value: values) {
			playerSimilarity += value.get();
		}

		context.write(new Text(key.getName()), new DoubleWritable(playerSimilarity));
	}
}
