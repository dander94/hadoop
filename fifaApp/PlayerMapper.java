package player;

import java.io.IOException;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.LongWritable;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import com.google.gson.*;

import static player.PlayerConfiguration.*;
import player.PlayerResult;

public class PlayerMapper extends Mapper <LongWritable, Text, PlayerResult, DoubleWritable> {

	private PlayerResult playerResult = new PlayerResult();
	public void map(LongWritable key, Text value,Context context) throws IOException,InterruptedException {
			Gson gson = new Gson();
			PlayerConfiguration playerConf = gson.fromJson(context.getConfiguration().get("playerConfiguration"), PlayerConfiguration.class);

			String valueString = value.toString();
			String[] row = valueString.split(",");

			Double modelValue = playerConf.compareRowAndPlayer(row);

			if(modelValue != null) {
				playerResult.setName(playerConf.getRowPlayerAndClub(row));
				playerResult.setValue(modelValue);

				context.write(playerResult, new DoubleWritable(modelValue));
			}
	}
}
