package player;

import java.sql.SQLException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.DriverManager;

import player.Player;

public class Connector {
  public static Player searchPlayer(String playerName, String playerClub) throws SQLException {
    Connection connect = DriverManager.getConnection("jdbc:postgresql://localhost:5432/players_db", "hadoop", "hadoop");   //connecting to postgres database
    Statement state = connect.createStatement();
    String query = String.format("select * from player where name='%s' and club='%s'", playerName, playerClub);
    ResultSet players = state.executeQuery(query);
    while (players.next()) {
        return new Player(
          players.getString(1),
          players.getString(2),
          players.getInt(3),
          players.getInt(4),
          players.getString(5),
          players.getString(6),
          players.getInt(7),
          players.getInt(8),
          players.getInt(9),
          players.getInt(10),
          players.getInt(11),
          players.getInt(12),
          players.getInt(13),
          players.getInt(14),
          players.getInt(15),
          players.getInt(16),
          players.getInt(17),
          players.getInt(18),
          players.getInt(19),
          players.getInt(20),
          players.getInt(21),
          players.getInt(22),
          players.getInt(23),
          players.getInt(24),
          players.getInt(25),
          players.getInt(26),
          players.getInt(27),
          players.getInt(28),
          players.getInt(29),
          players.getInt(30),
          players.getInt(31),
          players.getInt(32),
          players.getInt(33),
          players.getInt(34),
          players.getInt(35),
          players.getInt(36),
          players.getInt(37),
          players.getInt(38),
          players.getInt(39),
          players.getInt(40)
        );
      }
    return null;
  }
}
