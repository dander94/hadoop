package player;

import player.PlayerResult;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.mapreduce.Partitioner;

public class PlayerResultPartitioner extends Partitioner<PlayerResult, DoubleWritable> {

   @Override
   public int getPartition(PlayerResult result, DoubleWritable value, int numberOfPartitions) {
         // make sure that partitions are non-negative
         System.out.println(result.getName() + ", " + result.getValue().toString() + " => " + Integer.valueOf(Math.abs(result.getName().hashCode() % numberOfPartitions)));
         return Math.abs(result.getName().hashCode() % numberOfPartitions);
      }
 }
