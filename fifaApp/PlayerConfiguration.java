package player;

import player.Player;
import player.Connector;

import java.beans.PropertyDescriptor;
import java.beans.IntrospectionException;
import java.lang.IllegalAccessException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;

public class PlayerConfiguration {

    private Player searchedPlayer;
    public static final String PLAYER_NAME = "Javi García";
    public static final String PLAYER_CLUB = "Real Betis";
    public static final Integer RESULTS_TOLERANCE = 50;


    public static final double MODIFIER_POTENTIAL = 0;
    public static final double MODIFIER_CROSSING = 0;
    public static final double MODIFIER_FINISHING = 0;
    public static final double MODIFIER_HEADINGACCURACY = 0;
    public static final double MODIFIER_SHORTPASSING = 0;
    public static final double MODIFIER_VOLLEYS = 0;
    public static final double MODIFIER_DRIBBLING = 0;
    public static final double MODIFIER_CURVE = 0;
    public static final double MODIFIER_FKACCURACY = 0;
    public static final double MODIFIER_LONGPASSING = 0;
    public static final double MODIFIER_BALLCONTROL = 0;
    public static final double MODIFIER_ACCELERATION = 0;
    public static final double MODIFIER_SPRINTSPEED = 0;
    public static final double MODIFIER_AGILITY = 0;
    public static final double MODIFIER_REACTIONS = 0;
    public static final double MODIFIER_BALANCE = 0;
    public static final double MODIFIER_SHOTPOWER = 0;
    public static final double MODIFIER_JUMPING = 0;
    public static final double MODIFIER_STAMINA = 0;
    public static final double MODIFIER_STRENGTH = 0;
    public static final double MODIFIER_LONGSHOTS = 0;
    public static final double MODIFIER_AGGRESSION = 0;
    public static final double MODIFIER_INTERCEPTIONS = 0;
    public static final double MODIFIER_POSITIONING = 0;
    public static final double MODIFIER_VISION = 0;
    public static final double MODIFIER_PENALTIES = 0;
    public static final double MODIFIER_COMPOSURE = 0;
    public static final double MODIFIER_MARKING = 0;
    public static final double MODIFIER_STANDINGTACKLE = 0;
    public static final double MODIFIER_SLIDINGTACKLE = 0;
    public static final double MODIFIER_GKDIVING = 0;
    public static final double MODIFIER_GKHANDLING = 0;
    public static final double MODIFIER_GKKICKING = 0;
    public static final double MODIFIER_GKPOSITIONING = 0;
    public static final double MODIFIER_GKREFLEXES = 0;


    public HashMap<String, Double> attributesModifiers;
    public HashMap<String, Integer> attributeToIndex;

    public PlayerConfiguration(){

    }

    public PlayerConfiguration(Player player, HashMap<String, Double> attMod, HashMap<String, Integer> attInd){
      this.searchedPlayer = player;
      this.attributesModifiers = attMod;
      this.attributeToIndex = attInd;
    }


    public void initConfiguration() throws SQLException, Exception {
      this.searchPlayer();

      attributesModifiers = new HashMap<>();
      attributeToIndex = new HashMap<>();

      attributeToIndex.put("id", 0);

      attributeToIndex.put("name", 2); // Trick. Id has a comma.
      attributeToIndex.put("age", 3);
      attributeToIndex.put("potential", 4);
      attributeToIndex.put("club", 5);
      attributeToIndex.put("position", 6);
      attributeToIndex.put("crossing", 7);
      attributeToIndex.put("finishing", 8);
      attributeToIndex.put("headingaccuracy", 9);
      attributeToIndex.put("shortpassing", 10);
      attributeToIndex.put("volleys", 11);
      attributeToIndex.put("dribbling", 12);
      attributeToIndex.put("curve", 13);
      attributeToIndex.put("fkaccuracy", 14);
      attributeToIndex.put("longpassing", 15);
      attributeToIndex.put("ballcontrol", 16);
      attributeToIndex.put("acceleration", 17);
      attributeToIndex.put("sprintspeed", 18);
      attributeToIndex.put("agility", 19);
      attributeToIndex.put("reactions", 20);
      attributeToIndex.put("balance", 21);
      attributeToIndex.put("shotpower", 22);
      attributeToIndex.put("jumping", 23);
      attributeToIndex.put("stamina", 24);
      attributeToIndex.put("strength", 25);
      attributeToIndex.put("longshots", 26);
      attributeToIndex.put("aggression", 27);
      attributeToIndex.put("interceptions", 28);
      attributeToIndex.put("positioning", 29);
      attributeToIndex.put("vision", 30);
      attributeToIndex.put("penalties", 31);
      attributeToIndex.put("composure", 32);
      attributeToIndex.put("marking", 33);
      attributeToIndex.put("standingtackle", 34);
      attributeToIndex.put("slidingtackle", 35);
      attributeToIndex.put("gkdiving", 36);
      attributeToIndex.put("gkhandling", 37);
      attributeToIndex.put("gkkicking", 38);
      attributeToIndex.put("gkpositioning", 39);
      attributeToIndex.put("gkreflexes", 40);

      attributesModifiers.put("potential", Double.valueOf(MODIFIER_POTENTIAL));
      attributesModifiers.put("crossing", Double.valueOf(MODIFIER_CROSSING));
      attributesModifiers.put("finishing", Double.valueOf(MODIFIER_FINISHING));
      attributesModifiers.put("headingaccuracy", Double.valueOf(MODIFIER_HEADINGACCURACY));
      attributesModifiers.put("shortpassing", Double.valueOf(MODIFIER_SHORTPASSING));
      attributesModifiers.put("volleys", Double.valueOf(MODIFIER_VOLLEYS));
      attributesModifiers.put("dribbling", Double.valueOf(MODIFIER_DRIBBLING));
      attributesModifiers.put("curve", Double.valueOf(MODIFIER_CURVE));
      attributesModifiers.put("fkaccuracy", Double.valueOf(MODIFIER_FKACCURACY));
      attributesModifiers.put("longpassing", Double.valueOf(MODIFIER_LONGPASSING));
      attributesModifiers.put("ballcontrol", Double.valueOf(MODIFIER_BALLCONTROL));
      attributesModifiers.put("acceleration", Double.valueOf(MODIFIER_ACCELERATION));
      attributesModifiers.put("sprintspeed", Double.valueOf(MODIFIER_SPRINTSPEED));
      attributesModifiers.put("agility", Double.valueOf(MODIFIER_AGILITY));
      attributesModifiers.put("reactions", Double.valueOf(MODIFIER_REACTIONS));
      attributesModifiers.put("balance", Double.valueOf(MODIFIER_BALANCE));
      attributesModifiers.put("shotpower", Double.valueOf(MODIFIER_SHOTPOWER));
      attributesModifiers.put("jumping", Double.valueOf(MODIFIER_JUMPING));
      attributesModifiers.put("stamina", Double.valueOf(MODIFIER_STAMINA));
      attributesModifiers.put("strength", Double.valueOf(MODIFIER_STRENGTH));
      attributesModifiers.put("longshots", Double.valueOf(MODIFIER_LONGSHOTS));
      attributesModifiers.put("aggression", Double.valueOf(MODIFIER_AGGRESSION));
      attributesModifiers.put("interceptions", Double.valueOf(MODIFIER_INTERCEPTIONS));
      attributesModifiers.put("positioning", Double.valueOf(MODIFIER_POSITIONING));
      attributesModifiers.put("vision", Double.valueOf(MODIFIER_VISION));
      attributesModifiers.put("penalties", Double.valueOf(MODIFIER_PENALTIES));
      attributesModifiers.put("composure", Double.valueOf(MODIFIER_COMPOSURE));
      attributesModifiers.put("marking", Double.valueOf(MODIFIER_MARKING));
      attributesModifiers.put("standingtackle", Double.valueOf(MODIFIER_STANDINGTACKLE));
      attributesModifiers.put("slidingtackle", Double.valueOf(MODIFIER_SLIDINGTACKLE));
      attributesModifiers.put("gkdiving", Double.valueOf(MODIFIER_GKDIVING));
      attributesModifiers.put("gkhandling", Double.valueOf(MODIFIER_GKHANDLING));
      attributesModifiers.put("gkkicking", Double.valueOf(MODIFIER_GKKICKING));
      attributesModifiers.put("gkpositioning", Double.valueOf(MODIFIER_GKPOSITIONING));
      attributesModifiers.put("gkreflexes", Double.valueOf(MODIFIER_GKREFLEXES));
    }

    public void searchPlayer() throws SQLException, Exception{
      this.searchedPlayer = Connector.searchPlayer(PLAYER_NAME, PLAYER_CLUB);

      if (this.searchedPlayer == null) {
        throw new Exception("Player not found!");
      }
    }


    public static Integer safeParseValue(String value) {
        return Integer.valueOf(value.split("\\+")[0]);
    }

    private Integer getPlayerValueFor(String comparableProperty) throws IllegalAccessException, IntrospectionException, InvocationTargetException {
      PropertyDescriptor pd = new PropertyDescriptor(comparableProperty, Player.class);
      Method getter = pd.getReadMethod();
      return (Integer) getter.invoke(this.searchedPlayer);
    }

    public String getRowPlayerAndClub(String[] row) {
      return row[this.attributeToIndex.get("name")] + " - " + row[this.attributeToIndex.get("club")];
    }

    public Double compareRowAndPlayer(String[] row) {
      if (row[this.attributeToIndex.get("name")] == PLAYER_NAME && row[this.attributeToIndex.get("club")] == PLAYER_CLUB) {
        return null;
      }

      Double compareValue = 0.0;
      Iterator<String> comparableAttributesIterator = this.attributesModifiers.keySet().iterator();
      String compareKey;

      while(comparableAttributesIterator.hasNext()) {
        compareKey = comparableAttributesIterator.next();

        try {
          compareValue += Double.valueOf((this.getPlayerValueFor(compareKey) - Integer.valueOf(row[this.attributeToIndex.get(compareKey)])) * this.attributesModifiers.get(compareKey));
        } catch(Exception e) {
          e.printStackTrace();
          return null;
        }

        if (compareValue > RESULTS_TOLERANCE || compareValue < (-1*RESULTS_TOLERANCE)) {
          return null;
        }
      }

      return compareValue;

    }
}
