package player;

import org.apache.hadoop.io.*;
import java.io.DataOutput;
import java.io.DataInput;
import java.io.IOException;

public class PlayerResult implements WritableComparable<PlayerResult>{
  private String name;
  private Double value;

  public PlayerResult() {

  }

  public PlayerResult(String name, Double value) {
    this.name = name;
    this.value = value;
  }

  public String getName() {
    return this.name;
  }

  public Double getValue() {
    return this.value;
  }

  public void setName(String val) {
    this.name = val;
  }

  public void setValue(Double val) {
    this.value = val;
  }

  @Override
   public void write(DataOutput out) throws IOException {
       out.writeUTF(this.name);
       out.writeDouble(this.value);

   }

   @Override
   public void readFields(DataInput in) throws IOException {
       this.name = in.readUTF();
       this.value = in.readDouble();
   }

  @Override
  public int compareTo(PlayerResult compareObject) {
      int compareValue = this.name.compareTo(compareObject.getName());
      if (compareValue == 0) {
          compareValue = this.value.compareTo(compareObject.getValue());
      }
      return compareValue;
  }

  public String toString() {
    return this.name + ", " + this.value.toString();
  }
}
