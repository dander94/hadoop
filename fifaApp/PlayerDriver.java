package player;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.fs.Path;

import com.google.gson.*;

import player.PlayerConfiguration;

public class PlayerDriver extends Configured implements Tool {

 public int run(String[] args) throws Exception {
     Configuration conf = getConf();

     Gson gson = new Gson();

     PlayerConfiguration playerConf = new PlayerConfiguration();
     playerConf.initConfiguration();
     conf.set("playerConfiguration", gson.toJson(playerConf));

     Job job = new Job(conf);
     job.setJarByClass(PlayerDriver.class);
     job.setJobName("PlayerDriver");

     Path inputPath = new Path(args[0]);
     Path outputPath = new Path(args[1]);
     FileInputFormat.setInputPaths(job, inputPath);
     FileOutputFormat.setOutputPath(job, outputPath);

     job.setMapOutputKeyClass(player.PlayerResult.class);
     job.setOutputKeyClass(Text.class);
     job.setOutputValueClass(DoubleWritable.class);
     job.setMapperClass(player.PlayerMapper.class);
     job.setReducerClass(player.PlayerReducer.class);
     job.setPartitionerClass(player.PlayerResultPartitioner.class);
     job.setSortComparatorClass(player.PlayerResultSortComparator.class);
     job.setCombinerKeyGroupingComparatorClass(player.PlayerResultGroupingComparator.class);

     boolean status = job.waitForCompletion(true);
     return status ? 0 : 1;
 }

 /**
 * The main driver for the secondary sort MapReduce program.
 * Invoke this method to submit the MapReduce job.
 * @throws Exception when there are communication
 * problems with the job tracker.
 */
 public static void main(String[] args) throws Exception {
     // Make sure there are exactly 2 parameters
     if (args.length != 2) {
         throw new IllegalArgumentException("Usage: PlayerDriver" +
                                            " <input-path> <output-path>");
     }

     int returnStatus = ToolRunner.run(new PlayerDriver(), args);
     System.exit(returnStatus);
 }
}
