package player;

import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;

public class PlayerResultGroupingComparator extends WritableComparator {

    protected PlayerResultGroupingComparator() {
        super(PlayerResult.class, true);
    }

     @Override
     public int compare(WritableComparable wc1, WritableComparable wc2) {
         PlayerResult result = (PlayerResult) wc1;
         PlayerResult result2 = (PlayerResult) wc2;
         return result.getName().compareTo(result2.getName());
     }
 }
