cd

$HADOOP_HOME/bin/hdfs dfs -rm -r /mapreduce_output_player
$HADOOP_HOME/bin/hdfs dfs -rm -r /inputMapReduce

rm -rf fifaMapReduce

cp -r /home/vagrant/fifa/fifaApp fifaMapReduce

export CLASSPATH="/home/hduser_/fifaMapReduce/lib/gson.jar:/home/hduser_/fifaMapReduce/lib/hadoop-annotations.jar:$HADOOP_HOME/share/hadoop/mapreduce/hadoop-mapreduce-client-core-2.8.3.jar:$HADOOP_HOME/share/hadoop/mapreduce/hadoop-mapreduce-client-common-2.8.3.jar:$HADOOP_HOME/share/hadoop/common/hadoop-common-2.8.3.jar:~/fifaMapReduce/player/*:$HADOOP_HOME/lib/*:/home/hduser_/hive/lib/*"

echo "$CLASSPATH";

cd fifaMapReduce
javac -d . *.java
jar cfm ../Player.jar Manifest.txt player/*.class


$HADOOP_HOME/bin/hdfs dfs -copyFromLocal ~/inputMapReduce /
$HADOOP_HOME/bin/hadoop jar ../Player.jar /inputMapReduce /mapreduce_output_player

cd /home/vagrant/fifa

$HADOOP_HOME/bin/hdfs dfs -cat /mapreduce_output_player/part-r-00000

echo "*************************"
echo "*         DONE          *"
echo "*************************"
