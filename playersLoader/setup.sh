sudo su vagrant << EOF

# PostgreSQL
sudo apt-get -y install python-dev
sudo apt-get -y install libpq-dev
sudo apt-get -y install libffi-dev libssl-dev libxml2-dev libxslt1-dev
sudo apt-get -y install libxmlsec1 libxmlsec1-dev swig
sudo apt-get -y install libjpeg-dev libpng12-dev
sudo apt-get -y install python-lxml python-cffi libcairo2 libpango1.0-0 libgdk-pixbuf2.0-0 shared-mime-info

sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt/ $(lsb_release -cs)-pgdg main" >> /etc/apt/sources.list.d/pgdg.list'
sudo apt install ca-certificates
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add
sudo apt update
sudo apt-get -y install postgresql-9.4 postgresql-server-dev-9.4 pgadmin3 python-psycopg2
sudo echo "local all postgres peer" > /etc/postgresql/9.4/main/pg_hba.conf
sudo echo "local credibility_db django md5" >> /etc/postgresql/9.4/main/pg_hba.conf
sudo echo "local all all peer" >> /etc/postgresql/9.4/main/pg_hba.conf
sudo echo "host all all 127.0.0.1/32 md5" >> /etc/postgresql/9.4/main/pg_hba.conf
sudo echo "host all all ::1/128 md5" >> /etc/postgresql/9.4/main/pg_hba.conf

sudo service postgresql restart

sudo -u postgres bash -c "psql -c \"CREATE USER hadoop WITH PASSWORD 'hadoop';\""
sudo -u postgres createdb --owner=hadoop --encoding=UTF8 players_db


# Virtualenv
sudo apt-get -y install python3-pip

sudo apt-get -y install python3-venv


python3 -m venv players
source players/bin/activate

pip3 install wheel

pip install -r requirements.txt
sudo chown -R hduser_ ../playersLoader
EOF
