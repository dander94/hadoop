# -*- coding: utf-8 -*-

import csv

def safe_row(row):
    if row == "":
        raise ValueError
    return row.split("+")[0].replace("'", "''")

with open('/home/hduser_/adapted_data.csv', 'w') as result_file:
    with open('data.csv') as data_file:
        csv_reader = csv.reader(data_file, delimiter=',')
        csv_writer = csv.writer(result_file)
        line_count = 0

        for row in csv_reader:
            if line_count == 0:
                line_count += 1
            else:
                try:
                    data_values = [",".join([row[0], row[1]]), safe_row(row[2]), safe_row(row[3]), safe_row(row[8]), safe_row(row[9]), safe_row(row[21]), safe_row(row[54]), safe_row(row[55]),
                    safe_row(row[56]), safe_row(row[57]), safe_row(row[58]), safe_row(row[59]), safe_row(row[60]), safe_row(row[61]),
                    safe_row(row[62]), safe_row(row[63]), safe_row(row[64]), safe_row(row[65]), safe_row(row[66]), safe_row(row[67]),
                    safe_row(row[68]), safe_row(row[69]), safe_row(row[70]), safe_row(row[71]), safe_row(row[72]), safe_row(row[73]),
                    safe_row(row[74]), safe_row(row[75]), safe_row(row[76]), safe_row(row[77]), safe_row(row[78]), safe_row(row[79]),
                    safe_row(row[80]), safe_row(row[81]), safe_row(row[82]), safe_row(row[83]), safe_row(row[84]), safe_row(row[85]),
                    safe_row(row[86]), safe_row(row[87])]
                    csv_writer.writerow(data_values)
                except ValueError:
                    pass
