# -*- coding: utf-8 -*-

import csv
import psycopg2
from psycopg2 import Error

def safe_row(row):
    if row == "":
        raise ValueError
    return row.split("+")[0].replace("'", "''")

try:
    connection = psycopg2.connect(user = "hadoop",
                                  password = "hadoop",
                                  host = "127.0.0.1",
                                  port = "5432",
                                  database = "players_db")

    cursor = connection.cursor()

    create_table_query = '''CREATE TABLE player (
    ID VARCHAR (50) PRIMARY KEY,
    Name VARCHAR (255) NOT NULL,
    Age SMALLINT NOT NULL,
    Potential  SMALLINT NOT NULL,
    Club  VARCHAR (255) NOT NULL,
    Position  VARCHAR (3) NOT NULL,
    Crossing  SMALLINT NOT NULL,
    Finishing  SMALLINT NOT NULL,
    HeadingAccuracy  SMALLINT NOT NULL,
    ShortPassing  SMALLINT NOT NULL,
    Volleys  SMALLINT NOT NULL,
    Dribbling  SMALLINT NOT NULL,
    Curve  SMALLINT NOT NULL,
    FKAccuracy  SMALLINT NOT NULL,
    LongPassing  SMALLINT NOT NULL,
    BallControl  SMALLINT NOT NULL,
    Acceleration  SMALLINT NOT NULL,
    SprintSpeed  SMALLINT NOT NULL,
    Agility  SMALLINT NOT NULL,
    Reactions  SMALLINT NOT NULL,
    Balance  SMALLINT NOT NULL,
    ShotPower  SMALLINT NOT NULL,
    Jumping  SMALLINT NOT NULL,
    Stamina  SMALLINT NOT NULL,
    Strength  SMALLINT NOT NULL,
    LongShots  SMALLINT NOT NULL,
    Aggression  SMALLINT NOT NULL,
    Interceptions  SMALLINT NOT NULL,
    Positioning  SMALLINT NOT NULL,
    Vision  SMALLINT NOT NULL,
    Penalties  SMALLINT NOT NULL,
    Composure  SMALLINT NOT NULL,
    Marking  SMALLINT NOT NULL,
    StandingTackle  SMALLINT NOT NULL,
    SlidingTackle  SMALLINT NOT NULL,
    GKDiving  SMALLINT NOT NULL,
    GKHandling  SMALLINT NOT NULL,
    GKKicking  SMALLINT NOT NULL,
    GKPositioning  SMALLINT NOT NULL,
    GKReflexes  SMALLINT NOT NULL
);'''

    cursor.execute(create_table_query)
    print("Table players created successfully")

    with open('data.csv') as data_file:
        csv_reader = csv.reader(data_file, delimiter=',')
        line_count = 0
        for row in csv_reader:
            if line_count == 0:
                line_count += 1
            else:
                try:
                    cursor.execute(
                        """INSERT INTO player
                        VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', '{10}', '{11}',
                        '{12}', '{13}', '{14}', '{15}', '{16}', '{17}', '{18}', '{19}', '{20}', '{21}',
                        '{22}', '{23}', '{24}', '{25}', '{26}', '{27}', '{28}', '{29}', '{30}', '{31}',
                        '{32}', '{33}', '{34}', '{35}', '{36}', '{37}', '{38}', '{39}'
                        )""".format(
                            ",".join([row[0], row[1]]), safe_row(row[2]), safe_row(row[3]), safe_row(row[8]), safe_row(row[9]), safe_row(row[21]), safe_row(row[54]), safe_row(row[55]),
                            safe_row(row[56]), safe_row(row[57]), safe_row(row[58]), safe_row(row[59]), safe_row(row[60]), safe_row(row[61]),
                            safe_row(row[62]), safe_row(row[63]), safe_row(row[64]), safe_row(row[65]), safe_row(row[66]), safe_row(row[67]),
                            safe_row(row[68]), safe_row(row[69]), safe_row(row[70]), safe_row(row[71]), safe_row(row[72]), safe_row(row[73]),
                            safe_row(row[74]), safe_row(row[75]), safe_row(row[76]), safe_row(row[77]), safe_row(row[78]), safe_row(row[79]),
                            safe_row(row[80]), safe_row(row[81]), safe_row(row[82]), safe_row(row[83]), safe_row(row[84]), safe_row(row[85]),
                            safe_row(row[86]), safe_row(row[87])
                        )
                    )
                except ValueError:
                    pass


    connection.commit()
    print("Players are loaded!")
except (Exception, psycopg2.DatabaseError) as error :
    print ("Error while creating PostgreSQL table", error)
finally:
    #closing database connection.
        if(connection):
            cursor.close()
            connection.close()
