# Fifa Players Comparator

Suite to compare FIFA players by attributes using Hadoop MapReduce.

### Requirements
- VirtualBox
- Vagrant

### Tech Included
- Hadoop
- Hive
- Sqoop
- MySQL
- PostgreSQL

### Basic Setup
- Run `vagrant up --provision`
- Run `vagrant ssh` (From now on. Everything should be done inside vagrant)
- Run `cd config`
- Run `./finishSetup.sh`

### Hadoop
- Run `config/startHadoop.sh`

### Prepare PlayersDB and Files
- Run `cd /home/vagrant/fifa/playersLoader`
- Run `./setup.sh`
- Run `./runLoader.sh` (This will create a Table player in PostgreSQL with all the players info)
- Run `./runAdapt.sh` (Ignore rm errors) (This will create the file that MapReduce will use)

### Configure Comparator
- Edit `/home/vagrant/fifa/fifaApp/PlayerConfiguration.java`

Here you can configure the data that MapReduce will use.

Important variables:
- PLAYER_NAME -> Player to compare
- PLAYER_CLUB -> Club of the player to compare
- RESULTS_TOLERANCE -> This will limit the maximum difference shown.
- MODIFIER_* -> This is used to ponderate the result. For each attribute, setting 0 means that it won't be taken into account. Setting any value >0 will make the field be taken into account. This is used to determine how important is an attribute.

# Run FifaPlayer comparator
Please, ensure hadoop is started and all the steps above are completed.
- Run `cd /home/vagrant/fifa/`
- Run `./runFifa.sh`

# (Optional) Import data into Hive.
If you want to use Hive to make queries, you can import the data there.

- Run `cd /home/vagrant/fifa/importPlayers`
- Run `./hiveImport.sh`

To verify it works:
- Run `hive`
- Run `show tables;`

The output must be:
```
hive> show tables;
OK
player
```
