# cluster
$HADOOP_HOME/sbin/start-dfs.sh

# yarn
$HADOOP_HOME/sbin/start-yarn.sh

hadoop dfsadmin -safemode leave
