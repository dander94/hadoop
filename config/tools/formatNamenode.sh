cd ~/hadoop/etc/hadoop
sudo rm -rf /app
sudo mkdir -p /app/hadoop/tmp
sudo chown -R hduser_:hadoop_ /app/hadoop/tmp
sudo chmod 750 /app/hadoop/tmp

sudo rm -rf /home/hduser_/hdfs
sudo mkdir -p /home/hduser_/hdfs
sudo chown -R hduser_:hadoop_ /home/hduser_/hdfs
sudo chmod 750 /home/hduser_/hdfs

$HADOOP_HOME/bin/hdfs namenode -format
