cd $HIVE_HOME/scripts/metastore/upgrade/mysql/
sudo mysql -u root -proot -Bse "CREATE DATABASE metastore;USE metastore;SOURCE $HIVE_HOME/scripts/metastore/upgrade/mysql/hive-schema-2.3.0.mysql.sql;flush privileges;"
sudo mysql -u root -proot -Bse "USE metastore;CREATE USER 'hiveuser'@'localhost' IDENTIFIED BY 'hivepassword';GRANT all on *.* to 'hiveuser'@localhost identified by 'hivepassword';flush privileges;"
